import firebase from 'firebase';

export class ShareService {  
 
    app: firebase.app.App;
    user: any;
    
    
    constructor() {
        this.app=null;
    }
 
    setApp(app) {
        this.app=app;
    }
 
    getApp() {
        return this.app;
    }   

    getUser(){
        return this.user; 
    }
    
    setUser(user){
        this.user = user;
    }

}