import { Component } from '@angular/core';
import { NavController , AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import firebase from 'firebase';
import {ShareService} from '../Services/ShareService';
import { TranslateService } from 'ng2-translate';
var auth;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    
    password:any = "";
    email:any = "";
    
    constructor(public navCtrl: NavController, public alertCtrl: AlertController, private ShareService: ShareService,
             public translateService: TranslateService) {
    
  	var app = ShareService.getApp();
  	var database = app.database();
  	auth = app.auth();
    auth.onAuthStateChanged(function(user){
    if(user){
        if(user.displayName == null){
            ShareService.setUser(user.email);
        }
        else{
            ShareService.setUser(user.displayName);
            }
    }
    else{}
    })
    }
    CreateUser(): void{
        auth.createUserWithEmailAndPassword(this.email,this.password).then((authData) => {
            console.log(authData);
            let prompt = this.alertCtrl.create({
                title: 'Success',
                subTitle: 'Your new Account was created!',
                buttons: [{
                    text: 'Ok',
                    handler: data => {}
                }]
                
            });
            prompt.present();
        }).catch(function(error) {
            console.log(error);})
            let prompt = this.alertCtrl.create({
                title: 'Fail',
                subTitle: 'Something is wrong',
                buttons: [{
                    text: 'Ok',
                    handler: data => {}
                }]
                
            });
            prompt.present();
    }
    
    LoginFb(): void{
        auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((authData) => {
            this.navCtrl.setRoot(TabsPage);
            console.log(authData);
        }).catch(function(error) {
            console.log(error);
            let prompt = this.alertCtrl.create({
                title: 'Fail',
                subTitle: 'Something is wrong',
                buttons: [{
                    text: 'Ok',
                    handler: data => {}
                }]
                
            });
            prompt.present();
            });
    }
    
    LoginEmail(): any{
        return auth.signInWithEmailAndPassword(this.email,this.password).then((authData) => {
            this.navCtrl.setRoot(TabsPage);
            console.log(authData);
        }).catch(function(error) {
            console.log(error);
            let prompt = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something is wrong',
                buttons: [{
                    text: 'Ok',
                    handler: data => {}
                }]
                
            });
            prompt.present();
            });
            
    }
    
    toSpanish(){
      this.translateService.use('es');
    }

    toEnglish(){
      this.translateService.use('en');
    }

}
