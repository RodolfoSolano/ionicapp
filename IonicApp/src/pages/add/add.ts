import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';
import {ShareService} from '../Services/ShareService';

var databaseRef;
var auth;
var storage;
var storageRef;
var uploadTask;

@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
    cat:any;
    gen:any;
constructor(public navCtrl: NavController ,  private ShareService: ShareService) {
    var app = ShareService.getApp();
  	var database = app.database();
  	auth = app.auth();
  	storage = app.storage();
  	databaseRef = database.ref().child("categories");
}
    addCat(){
        this.cat="";
    }
    addGender(){
        var gen = {gender:this.gen};
        databaseRef.child(this.cat).push().set(gen);
        this.gen="";
    }
}