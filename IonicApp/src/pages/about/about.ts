import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';
import {ShareService} from '../Services/ShareService';
import { TranslateService } from 'ng2-translate';

var databaseRef;
var auth;
var storage;
var storageRef;
var uploadTask;
var msgRef;
var currentChat;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

	valor:any;
	username:any;
  to:any;
	
  	

constructor(public navCtrl: NavController, public translateService: TranslateService,  private ShareService: ShareService) {
  	
    //https://webflow.com/design/rodolfo-solanos-first-project?t=1&b=0
  	var app = ShareService.getApp();
  	var database = app.database();
  	auth = app.auth();
  	storage = app.storage();
  	databaseRef = database.ref().child("chat");
    msgRef = database.ref().child("messages");
    storageRef = storage.ref().child('chat_photos');

    databaseRef.on('child_added', function(snapshot) {
      
      var chat = snapshot.val();
      var to = chat.to;
      var from = chat.to;
      if(to == ShareService.getUser()){
        //if(is_already_added(ionItem,from)){}
          var listIon= document.getElementById("list");
          var p = document.createElement("p");
          var li = document.createElement("ion-item");
          var messageElm = document.createElement("span");
          li.appendChild(document.createTextNode(from));
          listIon.appendChild(p);
          p.appendChild(li);
          li.scrollIntoView(false);
      }
    });

    msgRef.on('child_added', function(snapshot) {
      // Get the chat message from the snapshot and add it to the UI
      /*var chat = snapshot.val();
      var to=chat.to;
      var from = chat.name;
      if(currentChat == from){
        if(to == ShareService.getUser() || from == ShareService.getUser()){
            
            var chat = snapshot.val();
            var listIon= document.getElementById("list");
            var p = document.createElement("p");
            var li = document.createElement("ion-item");

            if ( chat.message.indexOf("https://firebasestorage.googleapis.com/") == 0
            || chat.message.indexOf("https://lh3.googleusercontent.com/") == 0
            || chat.message.indexOf("http://pbs.twimg.com/") == 0
            || chat.message.indexOf("data:image/") == 0) {
              var usern=snapshot.child("name").val();
              li.appendChild(document.createTextNode(usern+": "));

              var imgElm = document.createElement("img");
              imgElm.src = chat.message;
              li.appendChild(imgElm);

            }
            else{
              var messageElm = document.createElement("span");
              var mensaje= snapshot.child("message").val();
              var usern=snapshot.child("name").val();
              li.appendChild(document.createTextNode(".........."+usern+": "+mensaje));
            }

            listIon.appendChild(p);
            p.appendChild(li);
            li.scrollIntoView(false);
        }
      }
      */
    });

    

  }

  	Send(): void{
      var reference = {from: this.ShareService.getUser() , to: this.to};
	    var chat = {name: this.ShareService.getUser() , message: this.valor, to: this.to};
      databaseRef.push().set(reference);
      msgRef.push().set(chat);
      this.valor = "";
      
  	}

    imgpicker(): void{
    var input: any = document.getElementById('fileInput');
    var file = input.files[0];
    
    // Get a reference to store file at photos/<FILENAME>.jpg
    var photoRef = storageRef.child(file.name);
    // Upload file to Firebase Storage
    uploadTask = photoRef.put(file);

  }

    onEnter(){this.Send();}
}
