import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';
import {ShareService} from '../Services/ShareService';

var databaseRef;
var auth;
var storage;
var storageRef;
var catRef;
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
    
    name:any;
    author:any;
    language:any;
    category:any;
    gender:any;
    price:any;
    
  constructor(public navCtrl: NavController, private ShareService: ShareService) {
    var app = ShareService.getApp();
  	var database = app.database();
  	auth = app.auth();
  	storage = app.storage();
  	catRef = database.ref().child("categories");
    databaseRef = database.ref().child("books");
    storageRef = storage.ref().child('books_photos');
    
    catRef.on("child_added",function(snapshot){
      var name= snapshot.key;
      console.log(name);
     });
    
  }
  AddBook():void{
    var book = {user: this.ShareService.getUser(), name: this.name , author: this.author , language: this.language , category: this.category , gender:this.gender};
    databaseRef.push().set(book);
    this.name = "";
    this.author = "";
    this.language = "";
    this.category = "";
    this.gender = "";
    this.price = "";
  }
  

}