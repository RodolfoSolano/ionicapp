import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';
import {ShareService} from '../Services/ShareService';

var databaseRef;
var auth;
var storage;
var storageRef;
var uploadTask;

@Component({
  selector: 'page-npi',
  templateUrl: 'npi.html',
})
export class Npi {

constructor(public navCtrl: NavController ,  private ShareService: ShareService) {
    var app = ShareService.getApp();
  	var database = app.database();
  	auth = app.auth();
  	storage = app.storage();
  	databaseRef = database.ref().child("books");
    storageRef = storage.ref().child('books_photos');
    
    databaseRef.on("child_added",function(snapshot){
      var name= snapshot.child("name").val();
      var author=snapshot.child("author").val();
      var language=snapshot.child("language").val();
      
  
      var listIon= document.getElementById("books");
    	var p = document.createElement("p");
    	var li = document.createElement("ion-item");
      li.appendChild(document.createTextNode("Book: "+name+"\n"+"Author: "+author+"\n"+"Lenguage: "+language));
      listIon.appendChild(p);
      p.appendChild(li);
      li.scrollIntoView(false);
     });
    
}
}