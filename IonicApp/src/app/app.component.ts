import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import firebase from 'firebase';
import {ShareService} from '../pages/Services/ShareService';

import { TranslateService } from 'ng2-translate';

import { HomePage } from '../pages/home/home';
import { AddPage } from '../pages/add/add';

@Component({
  templateUrl: 'app.html',
  providers: [ShareService]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private ShareService: ShareService,
             public translateService: TranslateService) {
    this.translateService.use('es');
    
    var config = {
    apiKey: "AIzaSyDsNNMH_nySfpch7kYmIOWPB8_YVxJyoDw",
    authDomain: "ionicapp-19108.firebaseapp.com",
    databaseURL: "https://ionicapp-19108.firebaseio.com",
    projectId: "ionicapp-19108",
    storageBucket: "ionicapp-19108.appspot.com",
    messagingSenderId: "536611992111"
    };

    var app = firebase.initializeApp(config);
    ShareService.setApp(app);
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
    logOut(){
        this.ShareService.getApp().auth().signOut();
        this.nav.setRoot(HomePage);
    }
    addCat(){
        this.nav.push(AddPage);
    }

    toSpanish(){
      this.translateService.use('es');
    }

    toEnglish(){
      this.translateService.use('en');
    }

}
